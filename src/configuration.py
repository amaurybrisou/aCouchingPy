#!/usr/bin/env python
#-*- coding:utf-8 -*-

import configparser
import os

class Configuration(configparser.ConfigParser):

    def __init__(self):
        super().__init__()
        self.config = self.read(os.path.dirname(os.path.realpath(__file__))+'/../config/config.cfg')