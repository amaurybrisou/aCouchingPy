#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-

from jsonrpc import dispatcher
from database import Database, _Account
 

class Account():

    @dispatcher.add_method
    def CreateAccount(**kwargs):
    	session = Database.session()

    	account = _Account(name=kwargs["name"])
    	
    	session.add(account)
    	session.commit()
    	account_id = account.account_id;
    	session.close()

    	return {"account_id" : account_id}
    	
    	