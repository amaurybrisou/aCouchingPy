#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-

from jsonrpc import dispatcher
from database import Database, _User
 

class User():

    @dispatcher.add_method
    def CreateUser(**kwargs):
        session = Database.session()

        user = _User(
            name=kwargs["name"],
            surname=kwargs["surname"],
            mail=kwargs["mail"],
            password=kwargs["password"],
            account_id=kwargs["account_id"]
            )

        session.add(user)
        session.commit()
        user_id = user.user_id
        session.close()


        return {"user_id" : user_id}