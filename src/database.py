#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-


from sqlalchemy import create_engine, MetaData, Table, Sequence, Column, Integer, String, ForeignKey
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import *
from configuration import Configuration


Base = declarative_base()


class Database():
    config = Configuration()
    conn = {
	'type': config.get('mysql' , 'driver'),
	'adap': config.get('mysql' , 'adap'),
	'host': config.get('mysql' , 'host'),
	'port': config.get('mysql' , 'port'),
	'user': config.get('mysql' , 'user'),
	'pass': config.get('mysql' , 'password'),
	'name': config.get('mysql' , 'db'),
    }
    url = '%(type)s+%(adap)s://%(user)s:%(pass)s@%(host)s:%(port)s/%(name)s' % conn

    engine =  create_engine(url, echo=True)
    session = sessionmaker(bind=engine)


    @staticmethod
    def create():
        Base.metadata.create_all(Database.engine, checkfirst=True)

    @staticmethod
    def drop():
        Base.metadata.drop_all(Database.engine, checkfirst=True)




class _Account(Base):
    __tablename__ = 'account'
    account_id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String(50))
    mysql_engine='InnoDB'

    def __repr__(self):
        return "<User(name='%s')>" % (self.name)


class _User(Base):
    __tablename__ = 'user'
    user_id = Column(Integer, autoincrement=True, primary_key=True)
    account_id = Column(Integer, ForeignKey("account.account_id"), nullable=False)
    name = Column(String(16), nullable = False)
    surname = Column(String(50), nullable=False)
    mail = Column(String(30), nullable=False)
    telephone = Column(String(50))
    password = Column(String(20), nullable = False)
    registered = Column(Integer)

    mysql_engine='InnoDB'


    def __repr__(self):
        return "<User(name='%s', account_id='%d', surname='%s', mail='%s', telephone='%s')>" % \
         (self.name, self.account_id, self.surname, self.mail, self.telephone)