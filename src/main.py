import socketserver
from jsonrpc import JSONRPCResponseManager, dispatcher
import json
import os
import http.server
import threading
from entity import Account, User


class JsonRPCServer(threading.Thread):
    def __init__(self, port ,address, HandlerClass):
        ServerClass = http.server.HTTPServer
        self.httpd = ServerClass((address, port), HandlerClass)

    def run(self):
        print("Serving on port", PORT)
        self.httpd.serve_forever()

    def shutdown(self):
        self.httpd.shutdown()
        self.httpd.socket.close()


class MyHandler(http.server.CGIHTTPRequestHandler ):
    def __init__(self, *args, **kwargs):
        os.chdir("../www")
        http.server.CGIHTTPRequestHandler .__init__(
            self, *args, **kwargs)

    def do_POST(self):
        varLen = int(self.headers['Content-Length'])
        post_data = self.rfile.read(varLen)
        # You now have a dictionary of the post data

        [print(c) for c in dispatcher]
        response = JSONRPCResponseManager.handle(
            post_data, dispatcher)

        self.send_response(200)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        self.flush_headers()

        self.wfile.write(response.json.encode("utf-8"))


PORT = 9999
httpd = JsonRPCServer(PORT, '127.0.0.1', MyHandler)
httpd.run()