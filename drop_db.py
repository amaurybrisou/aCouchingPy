#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-

import os, sys

path = os.getcwd()+'/src'
if not path in sys.path:
    sys.path.append(path)
del path

from database import *

Database.drop()
Database.create()